#include <stddef.h>

int main() {
	size_t n = 0;

	while(1) {
		n++;
		if(n >= 1000000000)
			break;
	}

	return 0;
}
