# simple-banchmarks

Speed benchmark design for simple algorithms in some languages

## Dependencies

- gcc 9.4.0 (for c and cpp)
- cargo 1.64.0 (for rust)
- GNU bash or other bash interpreter

## Usage

Clone the repostory

```
git clone https://gitlab.com/lpg2709/simple-banchmarks
cd simple-banchmarks
```

And run the script

```
./run.sh
```

## Tests

- *count1b/*: Count to 1 Billion with loop iteration;

## My env

- OS: Void Linux [whit my custom config](https://gitlab.com/lpg2709/lpg-linux/-/tree/Void)
- CPU: AMD FX6300
- RAM: 8 Gb
