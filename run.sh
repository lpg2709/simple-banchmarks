#!/bin/bash

allCSources=$(ls ./c | grep .c)
allCBin=()

allRustSources=$(ls ./rust)
allRustBin=()

default_folder=$(pwd)

if [[ $1 == "clean" ]]; then
	echo "> Cleaning"
	for i in ${allCSources[@]}; do
		bin=$(echo $i | sed 's/.c//g')
		rm -rf ./c/$bin
	done

	for i in ${allRustSources[@]}; do
		rm -rf ./rust/$i/target
	done

else
	for i in ${allCSources[@]}; do
		echo "Compiling: $i [c]"
		bin=$(echo $i | sed 's/.c//g')
		allCBin+=("$bin")
		gcc ./c/$i -o ./c/$bin -Wall -O3
	done

	for i in ${allrustsources[@]}; do
		echo "compiling: $i [rust]"
		cd ./rust/$i && cargo build --release
		cd $default_folder
		allrustbin+=("$i")
	done

	echo ""
	echo "--- Starting banchmark ---"
	for i in ${allCBin[@]}; do
		fileSize=$(ls -lah ./c/$i | awk '{print $5}')
		printf "\n> $i [c]\n"
		printf "Executable size: $fileSize\n"
		time ./c/$i
	done


	for i in ${allRustBin[@]}; do
		fileSize=$(ls -lah ./rust/$i/target/release/$i | awk '{print $5}')
		printf "\n> $i [rust]\n"
		printf "Executable size: $fileSize\n"
		time ./rust/$i/target/release/$i
	done

	echo "--------------------------"
fi

